// $.ajax({

//     url: "http://hackatontsdl.mybluemix.net/offers",
//     type: "GET",
//     crossDomain: true,
//     success: function(response){
//         response = response.filter(function(elemento){
//             return elemento.risk != "Confiable"
//         });
//         proccessData(response);
//         grafica1(response);
//         grafica2(response);
//     }
// });
function refresh(){
    $.ajax({
        
        url: "https://hackatontsdl.mybluemix.net/offers",
        type: "GET",
        crossDomain: true,
        success: function(response){
                response = response.filter(function(elemento){
                    return elemento.risk != "Confiable"
                });
                $("#tableBody").empty()
                proccessData(response);
                grafica1(response);
                grafica2(response);
            }
        });
    }
refresh()

function proccessData(data){
    for(var i = 0; i<data.length; i++){
        //if(data[i].risk != "Confiable"){
        var tr = document.createElement("tr");
        var riesgo = document.createElement("td");
        var spanRiesgo = document.createElement("span");

        if(data[i].risk == "Alto"){
            spanRiesgo.setAttribute("class", "label label-danger");
        }else if(data[i].risk == "Moderado"){
            spanRiesgo.setAttribute("class", "label label-warning");
        }else {
            spanRiesgo.setAttribute("class", "label label-info");
        }
        spanRiesgo.appendChild(document.createTextNode(data[i].risk));
        riesgo.appendChild(spanRiesgo);
        tr.appendChild(riesgo);

        var titulo = document.createElement("td");
        titulo.appendChild(document.createTextNode(data[i].title));
        tr.appendChild(titulo);

        var desc = document.createElement("td");
        desc.appendChild(document.createTextNode(data[i].description));
        tr.appendChild(desc);

        // var salario = document.createElement("td");
        // salario.appendChild(document.createTextNode(data[i].salary));
        // tr.appendChild(salario);

        var cat = document.createElement("td");
        cat.appendChild(document.createTextNode(data[i].category));
        tr.appendChild(cat);

        var email = document.createElement("td");
        email.appendChild(document.createTextNode(data[i].email));
        tr.appendChild(email);

        var url = document.createElement("td");

        var aButton = document.createElement("a");
        aButton.setAttribute("href", data[i].url);
        aButton.setAttribute("target", "_blank");
        var buttonUrl = document.createElement("button");
        buttonUrl.setAttribute("class", "btn btn-info");
        buttonUrl.setAttribute("type", "button");
        buttonUrl.appendChild(document.createTextNode("Visitar"));
        aButton.appendChild(buttonUrl);
        url.appendChild(aButton);
        tr.appendChild(url);

        $("#tableBody").append(tr);//}
    }
}

function filterTable(){
    var input, filter, select, selection, table, tr, td, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    select = document.getElementById("selection");
    selection = select.value;
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[selection];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function cleanFilter(){
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    input = document.getElementById("myInput");
    input.value = "";
    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        tr[i].style.display = "";
    }
}




function grafica1(data) {
    $("#piechart").empty()
    var svg = d3.select("#piechart"),
    width = +svg.attr("width"),
    height = +svg.attr("height"),
    radius = Math.min(width, height) / 2,
    g = svg.append("g").attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

var color = d3.scaleOrdinal(["#d9534f", "#f0ad4e", "#5bc0de", "#5cb85c"]);

var pie = d3.pie()
    .sort(null)
    .value(function(d) { return d.amount; });

var path = d3.arc()
    .outerRadius(radius - 10)
    .innerRadius(0);

var label = d3.arc()
    .outerRadius(radius - 40)
    .innerRadius(radius - 150);

    var json = {"Alto":0, "Moderado":0, "Leve":0};
    for(var i = 0; i<data.length; i++){

        json[data[i].risk] = json[data[i].risk]+1;
    }

    var arr = [];
    arr.push({"cat":"Alto", "amount":json["Alto"]});
    arr.push({"cat":"Moderado", "amount":json["Moderado"]});
    arr.push({"cat":"Leve", "amount":json["Leve"]});
    //arr.push({"cat":"Confiable", "amount":json["Confiable"]});
    var arc = g.selectAll(".arc")
        .data(pie(arr))
        .enter().append("g")
        .attr("class", "arc");

    arc.append("path")
        .attr("d", path)
        .attr("fill", function(d) {
            console.log(d);
            return color(d.data.cat); });

    arc.append("text")
        .attr("transform", function(d) { return "translate(" + label.centroid(d) + ")"; })
        .attr("dy", "0.35em")
        .text(function(d) { return d.data.cat+" - "+d.data.amount; });
}
