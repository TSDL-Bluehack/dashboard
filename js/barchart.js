


function grafica2(data) {

    $("#barchart").empty()
    var svgg = d3.select("#barchart"),
    marginn = {top: 20, right: 20, bottom: 30, left: 40},
    widthh = +svgg.attr("width") - marginn.left - marginn.right,
    heightt = +svgg.attr("height") - marginn.top - marginn.bottom;

var xx = d3.scaleBand().rangeRound([0, widthh]).padding(0.1),
    yy = d3.scaleLinear().rangeRound([heightt, 0]);

var gg = svgg.append("g")
    .attr("transform", "translate(" + marginn.left + "," + marginn.top + ")");

    var json = {"SinExperiencia":0, "Bachillerato":0, "SinEducacion":0};
    for(var i = 0; i<data.length; i++){

        json[data[i].category] = json[data[i].category]+1;
    }

    var arr = [];
    arr.push({"cat":"SinExperiencia", "amount":json["SinExperiencia"]});
    arr.push({"cat":"Bachillerato", "amount":json["Bachillerato"]});
    arr.push({"cat":"SinEducacion", "amount":json["SinEducacion"]});

    xx.domain(arr.map(function(d) { return d.cat+" - "+d.amount; }));
    yy.domain([0, d3.max(arr, function(d) { return d.amount; })]);

    gg.append("g")
        .attr("class", "axis axis--x")
        .attr("transform", "translate(0," + heightt + ")")
        .call(d3.axisBottom(xx));

    gg.append("g")
        .attr("class", "axis axis--y")
        .call(d3.axisLeft(yy).ticks(10))
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", "0.71em")
        .attr("text-anchor", "end")
        .text("Cantidad");

    gg.selectAll("text")
      .attr("font-size","15")

    gg.selectAll(".bar")
        .data(arr)
        .enter().append("rect")
        .attr("class", "bar")
        .attr("x", function(d) { return xx(d.cat+" - "+d.amount); })
        .attr("y", function(d) { return yy(d.amount); })
        .attr("width", xx.bandwidth())
        .attr("height", function(d) { return heightt - yy(d.amount); });

}