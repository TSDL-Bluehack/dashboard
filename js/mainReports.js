$.ajax({

    url: "https://hackatontsdl.mybluemix.net/reports",
    type: "GET",
    crossDomain: true,
    success: function(response){

        proccessData(response);
    }
});

function proccessData(data){
    for(var i = 0; i<data.length; i++){

        var tr = document.createElement("tr");
        var riesgo = document.createElement("td");
        var spanRiesgo = document.createElement("span");

        if(data[i].riesgo == "alto"){
            spanRiesgo.setAttribute("class", "label label-danger");
        }else if(data[i].riesgo == "moderado"){
            spanRiesgo.setAttribute("class", "label label-warning");
        }else {
            spanRiesgo.setAttribute("class", "label label-info");
        }
        spanRiesgo.appendChild(document.createTextNode(data[i].riesgo));
        riesgo.appendChild(spanRiesgo);
        tr.appendChild(riesgo);

        var nombre = document.createElement("td");
        nombre.appendChild(document.createTextNode(data[i].nombre));
        tr.appendChild(nombre);

        var ubicacion = document.createElement("td");
        ubicacion.appendChild(document.createTextNode(data[i].ubicacion));
        tr.appendChild(ubicacion);

        var atendido = document.createElement("td");
        if(data[i].atendido){

            var spanAtendido = document.createElement("span");
            spanAtendido.setAttribute("class", "label label-success");
            spanAtendido.appendChild(document.createTextNode("Atendido"));
            atendido.appendChild(spanAtendido);

        }
        else{
            var button = document.createElement("button");
            button.setAttribute("class", "btn btn-default");
            button.setAttribute("type", "button");
            button.appendChild(document.createTextNode("Atender"));
            button.setAttribute("onclick", "atender("+JSON.stringify(data[i])+")");
            atendido.appendChild(button);
        }
        tr.appendChild(atendido);
        $("#tableBody").append(tr);//}
    }
}

function atender(data) {
    data.atendido = true;

    $.ajax({
        url: "https://hackatontsdl.mybluemix.net/reports",
        type: "POST",
        data: data,
        complete:function(datos){ //success es una funcion que se utiliza si el servidor retorna informacion

            window.location.reload();
        },
        crossDomain: true,
        dataType: 'application/json'
    });

}

function filterTable(){
    var input, filter, select, selection, table, tr, td, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    select = document.getElementById("selection");
    selection = select.value;
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[selection];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function cleanFilter(){
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    input = document.getElementById("myInput");
    input.value = "";
    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        tr[i].style.display = "";
    }
}

